FROM php:7.2.24-fpm
RUN apt-get update \
    && apt-get install  -y \
    unzip \
    vim

RUN apt-get install curl -y
RUN apt-get install -y libcurl4-gnutls-dev \
    && apt-get install -y libicu-dev \
    && apt-get install -y libxslt-dev

RUN apt-get install -y libpng-dev

RUN docker-php-ext-install gd \
    && docker-php-ext-install pdo \
    && docker-php-ext-install opcache \
    && docker-php-ext-install mysqli \
    && docker-php-ext-install curl \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && docker-php-ext-install xsl \
    && docker-php-ext-install mbstring \
    && docker-php-ext-install zip \
    && docker-php-ext-install bcmath \
    && docker-php-ext-install soap \
    && docker-php-ext-install pdo_mysql

RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/local/bin --filename=composer

RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

RUN sed -i "s/memory_limit = .*/memory_limit = 1024M/" /usr/local/etc/php/php.ini \
    && sed -i "s/upload_max_filesize = .*/upload_max_filesize = 256M/" /usr/local/etc/php/php.ini \
    && sed -i "s/zlib.output_compression = .*/zlib.output_compression = on/" /usr/local/etc/php/php.ini \
    && sed -i "s/max_execution_time = .*/max_execution_time = 18000/" /usr/local/etc/php/php.ini \
    && sed -i "s/;date.timezone.*/date.timezone = UTC/" /usr/local/etc/php/php.ini \
    && sed -i "s/;opcache.save_comments.*/opcache.save_comments = 1/" /usr/local/etc/php/php.ini

EXPOSE 9000
